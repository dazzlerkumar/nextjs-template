This is a [Next.js](https://nextjs.org/) project design for internal use in Doorpix Projects.

### Getting Started

Clone the repo using:

```bash
git clone https://gitlab.com/dazzlerkumar/nextjs-template.git
```

Change the directory name from `nextjs-template` to your project name.

Go to file explorer and open project directory, find `.git` folder (make to unhide hidden items).
Then open `config` file in notepad and replace,
```
url = https://gitlab.com/dazzlerkumar/nextjs-template.git
```
to 
```
url = your project/'s url
```
Save it. Open VS Code and run command,
```
npm i
or
npm install
or 
yarn install
```